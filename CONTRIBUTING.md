# Contributing is easy!

All you gots to do is take the `template.md` file, rename it to the name of the
recipe then edit the file following the template! Once you've done that, make a 
pull and someone will review the recipe. 

Get creative too! Add images, reference `source code` (i.e. a recipe origins), 
or just make something silly for people to try! It's a cookbook by the people, 
for the people!